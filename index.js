const express = require('express')
const app = express()
const port = 3000
const userGameController = require('./controller');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use(express.urlencoded({extended: false}))
app.use(express.json())

app.get('/user_games', userGameController.list)
app.get('/user_games/:id',userGameController.getById)
app.post('/user_games',userGameController.create)
app.delete('/user_games/:id',userGameController.delete)
app.put('/user_games/:id',userGameController.update)

app.use('/api-docs', swaggerUi.serve);
app.get('/api-docs', swaggerUi.setup(swaggerDocument));


app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })