'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('user_games', [
     {
     username: 'ade_sug',
     email : 'adesugiantoro89@gmail.com',
     password : 'binaracademy123',
     createdAt: new Date(),
     updatedAt: new Date()
    },
    {
      username: 'aufa_alaina',
      email : 'alaina@gmail.com',
      password : 'binaracademy321',
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      username: 'rizal_khamam',
      email : 'rizal@gmail.com',
      password : 'binaracademy890',
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      username: 'user_86',
      email : 'user@gmail.com',
      password : 'binarAcademy',
      createdAt: new Date(),
      updatedAt: new Date()
     },
     {
      username: 'bagas_7',
      email : 'bagas@gmail.com',
      password : 'BINARACADEMY',
      createdAt: new Date(),
      updatedAt: new Date()
     },
  ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return queryInterface.bulkDelete('user_games', null, {truncate:true, restartIdentity:true});
  }
};
