const {user_games} = require('./models')
class userGameController {
    static async list(req,res) {
      try {
       let data = await  user_games.findAll({
          attributes: ['id', 'username', 'email'],
        })
        if (! data) {
          res.status(404).json({
            message: "Data Not Found"
          })
        }else{
          res.status(200).json(data)
        }   
      } catch (error) {
        res.status(500).json({
          message : "Internal Server Error"
        })
      }
       
        
    }
    static async getById(req,res) {
     try {
      let tes =  await user_games.findOne({
        where: {
            id:req.params.id
        }
    })
    if (tes == null) {
      res.status(404).json({
        message: "data not found"
      })
    }else{
      res.status(200).json(data)
  
    }
   
     } catch (error) {
      res.status(500).json({
        message: 'Internal Server Error'
      })
     }
           
     
        
    }
    static async create(req, res) {
      try {
      const created = await user_games.create({
        username : req.body.username,
        email : req.body.email,
        password : req.body.password
      })
   
        res.status(200).json({
          message: "Successfully create user",
          created
        })
      
      } catch (error) {
        res.status(500).json({
          message: "Internal Server Error",

        })
      }
    }
      static update(req,res) {
        user_games.update({
          username : req.body.username
        }, {
          where: {
            id:req.params.id
          },
          individualHooks:true, 
          returning:true,
        })
        .then((data) => {
          console.log(data);
          res.status(200).json({
            message: "successfully update data",
            data,
          })
        })
        .catch((error) => {
          res.status(500).json(error)
        })
      }
    
      static async delete (req, res) {
        try {
         let data = await user_games.destroy({
            where: {
              id: req.params.id
            }
          })
          if (!data) {
            res.status(400).json ({
              message : "data not found"
            })
          }else{
            res.status(200).json({
              message: 'Succesfully delete data',
            })
          }
        } catch (error) {
          res.status(500).json({
            message: 'Internal Server Error'
          })
        }
      }
}

module.exports = userGameController