'use strict';
const { user } = require('pg/lib/defaults');
const {
  Model
} = require('sequelize');

const caesar_encrypt = require('../encrypt')
module.exports = (sequelize, DataTypes) => {
  class user_games extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  user_games.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user_games',
  });

  user_games.addHook('beforeCreate',(user_games) => {
    // console.log(user_games);
   
      user_games.password = caesar_encrypt(user_games.password, 2)
  
  },
  user_games.addHook('beforeUpdate',(user_games)=> {
  
      user_games.password = caesar_encrypt(user_games.password, 2)

  }))
  return user_games;
};